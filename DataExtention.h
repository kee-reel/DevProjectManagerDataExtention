#pragma once

#include <QObject>
#include <QDebug>
#include <QString>
#include <QAbstractItemModel>

#include "../../Interfaces/Middleware/idataextention.h"
#include "../../Interfaces/Utility/i_dev_project_manager_data_extention.h"

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Middleware/DataExtentionBase/DataExtentionBase.h"

class DataExtention : public QObject, public IDevProjectManagerDataExtention
{
	Q_OBJECT
	Q_INTERFACES(IDevProjectManagerDataExtention IDataExtention)
	DATA_EXTENTION_BASE_DEFINITIONS(IDevProjectManagerDataExtention, IDevProjectManagerDataExtention, {"path", "repository"})

public:
	DataExtention(QObject* parent) :
		QObject(parent)
	{
	}

	virtual ~DataExtention() = default;

signals:
	void modelChanged() override;

public:
	QString path() override
	{
		return "";
	}
	QString repository() override
	{
		return "";
	}
};
